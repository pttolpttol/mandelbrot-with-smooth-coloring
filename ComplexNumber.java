
/**
 * ComplexNumber.java
 *
 * @author Shreyas Siravara, Neal Bhasin
 *
 */
public class ComplexNumber {

    public double a, bi;

    public ComplexNumber(double a, double bi) {
        this.a = a;
        this.bi = bi;
    }

    /**
     * Add this complex number to another complex number
     * @return ComplexNumber Z + ComplexNumber Zn
     */
    public ComplexNumber add(ComplexNumber Zn) {
        return new ComplexNumber(this.a + Zn.a, this.bi + Zn.bi);
    }

    public ComplexNumber subtract(ComplexNumber Zn) {
        return this.add(new ComplexNumber(-1 * Zn.a, -1 * Zn.bi));
    }

    public ComplexNumber multiply(ComplexNumber Zn) {
        return new ComplexNumber(this.a * Zn.a - this.bi * Zn.bi, this.a * Zn.bi + this.bi * Zn.a);
    }

    /**
     * Divide this complex number by another complex number
     * @return ComplexNumber Z / ComplexNumber Zn
     * [( Z.a*Zn.a + Z.bi*Zn.bi ) / (Zn.a^2 + Zn.bi^2)] + 
     * [( Z.bi*Zn.a - Z.a*Zn.bi ) / (Zn.a^2 + Zn.bi^2)]i
     */
    public ComplexNumber divide(ComplexNumber Zn) {
        return new ComplexNumber((this.a * Zn.a + this.bi * Zn.bi) / (this.a * this.a + Zn.bi * Zn.bi),
                (this.a * Zn.bi + this.bi * Zn.a) / (this.a * this.a + Zn.bi * Zn.bi));
    }

    /**
     * Return this complex number to the n-th power,
     * where n is any real number as per De Moivre's formula
     */
    public ComplexNumber raiseTo(double n) {
        double r = this.getMagnitude();					//radius r
        double theta = Math.atan2(this.bi, this.a); 	//theta - angle of point /  polar
        double r_to_n = Math.pow(r, n);				//r^n
        double n_theta = n * theta;						//n*theta
        return new ComplexNumber(r_to_n * Math.cos(n_theta), r_to_n * Math.sin(n_theta));
    }

    /**
     * Get the magnitude (r) of the complex number
     * @return r^2 = (a^2 + bi^2)
     */
    public double getMagnitude() {
        return Math.sqrt((this.a * this.a) + (this.bi * this.bi));
    }

    public double getModulus() {
        return this.getMagnitude();
    }

    /*
     * Perform z^x + c up to iter_max times, break out if z magnitude > 2.
     *  (where this ComplexNumber is c)
     */
    public int computeMandelbrot(int iter_max, int z_exp) {
        int iter_cur = 1;
        ComplexNumber z = new ComplexNumber(this.a, this.bi);
        while (z.getMagnitude() <= 2.0 && iter_cur < iter_max) {
            z = (z.raiseTo(z_exp)).add(this);
            iter_cur++;
        }
        return iter_cur;
    }

    @Override
    public boolean equals(Object another) {
        try {
            ComplexNumber Zn = (ComplexNumber) another;
            if (Zn.a == this.a && Zn.bi == this.bi) {
                return true;
            } else {
                return false;
            }
        } catch (Exception E) {
            return false;
        }
    }
}
