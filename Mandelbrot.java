
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.imageio.ImageIO;

public class Mandelbrot {

    private static final int MAX_ITERATIONS = 100;
    private static final int HEIGHT = 1024;
    private static final int WIDTH = 1024;
    private static final double ESCAPE_RADIUS = 2.0;
    private Graphics2D g2d = null;
    private BufferedImage fractalImage = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);

    public Mandelbrot() {
        g2d = fractalImage.createGraphics();
        computeFractal();
        createImage();
    }

    public void createImage() {
        String filename = "Mandelbrot_" + WIDTH + "x" + WIDTH + "_" + new SimpleDateFormat("dd-MM-yy_hh.mm.ss").format(Calendar.getInstance().getTime()) + ".png";
        try {
            ImageIO.write(fractalImage, "png", new File(filename));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void computeFractal() {

        for (int x = 0; x < WIDTH; x++) {

            for (int y = 0; y < HEIGHT; y++) {

                double cx = (double) x * 4 / (double) WIDTH - 2;
                double cyi = (double) y * 4 / (double) HEIGHT - 2;

                ComplexNumber Zc = new ComplexNumber(cx, cyi);

                /**Mandelbrot Computation loop*/
                int iterations = 0;
                ComplexNumber Zn = new ComplexNumber(0, 0);

                while (iterations < MAX_ITERATIONS && (Zn.getMagnitude() < ESCAPE_RADIUS)) {

                    Zn = Zn.raiseTo(2).add(Zc);

                    iterations++;

                }

                /**Smooth coloring algorithm*/
                double nsmooth = (iterations + 1 - Math.log(Math.log(Zn.getMagnitude())) / Math.log(ESCAPE_RADIUS));

                double smoothcolor = nsmooth / MAX_ITERATIONS;

                if (iterations < MAX_ITERATIONS) {
                    int rgb = Color.HSBtoRGB((float) (0.99f + 1.9 * smoothcolor), 0.9f, 0.9f);
                    g2d.setColor(new Color(rgb));
                } else {
                    g2d.setColor(Color.black.darker());
                }

                g2d.drawLine(x, y, x, y);
            }
        }
    }

    public double log(double base, double number) {
        return Math.log(number) / Math.log(base);
    }

    public static void main(String[] args) {
        new Mandelbrot();
    }
}
